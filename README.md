## RPG Character Screen

A screen that will display the various stats of an RPG character and their equipped items and weapons
similar to World of Warcraft's character screen while the mechanics will mirror Dungeons & Dragons
---

## REQUIREMENTS

1. An item shall be an item that can be equipped on the player.
2. An item can be either armour or a weapon.
3. A character shall have stats. A stat is a numerical attribute that affects what the character can/cannot do.
4. A character shall have a collection of the following stats:
	- HP (Health Points)
	- AC (Armor Class)	
	- Str (Determines Large weapon damage
	- Dex (Determines dagger/ Ranged damage/Initiative
	- Const (Determines Hit Points)
	- Wis (Determines Mana Points (MP))
	- Int (Determines Magic Damage)
	- Char (Determines persuasive skill)

5. A character shall have:
	- a race
	- a class
	- A background
	- 2 1 handed weapons slots/ 1 2 handed weapon slot.
	- 1 Armor Slot

6. In the character screen, a slot shall be the space that can allow an item to be equipped. Only an item that matches the slots item type can be equipped.

7. The character screen will display the following:
	- Each armor and weapon slot. 
	- A list of the character's stats. The stats will dynamically update based on the items the player has equipped.
